var clientesObtenidos;
function getClientes() {
  var url="https://services.odata.org/V4/Northwind/Northwind.svc/Customers";//?$filter=Country eq 'Germany'";
  var request= new XMLHttpRequest();
  request.onreadystatechange = function(){
    if(this.readyState==4&&this.status==200){
      //console.table(JSON.parse(request.responseText).value);//los valores de value parsealo y muestralos en un atabla
      clientesObtenidos=request.responseText;
      procesarClientes();
    }
  }

  request.open("GET", url, true);//abrir peticiòn
  request.send();//enviar peticiòn
}

function procesarClientes() {
  var JSONClientes=JSON.parse(clientesObtenidos);
  //alert(JSONProductos.value[0].ProductName);
  var rutaBandera="https://www.countries-ofthe-world.com/flags-normal/flag-of-"
  var divTabla=document.getElementById("divTablaClientes");
  var tabla=document.createElement("table");//crea un elemento tabla con js
  var tbody=document.createElement("tbody");//crea un elemento de la tabla
  tabla.classList.add("table");//asigna clase al elemento tabla para el estilo bootstrap
  tabla.classList.add("table-striped");

  for(var i=0;i<JSONClientes.value.length;i++){
    var nuevaFila=document.createElement("tr");//crea un elemento tr para la tabla
    var columnaNombre=document.createElement("td");//crea un elemento td para la tabla
    columnaNombre.innerText=JSONClientes.value[i].ContactName;
    var columnaCity=document.createElement("td");
    columnaCity.innerText=JSONClientes.value[i].City;
    var columnaBandera=document.createElement("td");
    var imgBandera=document.createElement("img");
    imgBandera.classList.add("flag");

    if (JSONClientes.value[i].Country=="UK") {
      imgBandera.src=rutaBandera+"United-Kingdom.png";
    }else {
      imgBandera.src=rutaBandera+JSONClientes.value[i].Country+".png";
    }


    //columnaCountry.innerText=JSONClientes.value[i].Country;
    columnaBandera.appendChild(imgBandera);

    nuevaFila.appendChild(columnaNombre);//a la variable nuevaFila le agragamos los valores
    nuevaFila.appendChild(columnaCity);
    nuevaFila.appendChild(columnaBandera);
    tbody.appendChild(nuevaFila)//agregamos los valores obtenidos de la nuevaFila al tbody
  }
  tabla.appendChild(tbody);//agregamos todas las filas que tiene tbody a la tabla
  divTabla.appendChild(tabla); //agragamos la tabla al div del html5
}
