var productosObtenidos;
function getProductos() {
  var url="https://services.odata.org/V4/Northwind/Northwind.svc/Products";
  var request= new XMLHttpRequest();
  request.onreadystatechange = function(){
    if(this.readyState==4&&this.status==200){
      //console.table(JSON.parse(request.responseText).value);//los valores de value parsealo y muestralos en un atabla
      productosObtenidos=request.responseText;
      procesarProductos();
    }
  }

  request.open("GET", url, true);//abrir peticiòn
  request.send();//enviar peticiòn
}

function procesarProductos() {
  var JSONProductos=JSON.parse(productosObtenidos);
  //alert(JSONProductos.value[0].ProductName);
  var divTabla=document.getElementById("divTablaProductos");
  var tabla=document.createElement("table");//crea un elemento tabla con js
  var tbody=document.createElement("tbody");//crea un elemento de la tabla
  tabla.classList.add("table");//asigna clase al elemento tabla para el estilo bootstrap
  tabla.classList.add("table-striped");

  for(var i=0;i<JSONProductos.value.length;i++){
    var nuevaFila=document.createElement("tr");//crea un elemento tr para la tabla
    var columnaNombre=document.createElement("td");//crea un elemento td para la tabla
    columnaNombre.innerText=JSONProductos.value[i].ProductName;
    var columnaPrecio=document.createElement("td");
    columnaPrecio.innerText=JSONProductos.value[i].UnitPrice;
    var columnaStock=document.createElement("td");
    columnaStock.innerText=JSONProductos.value[i].UnitsInStock;

    nuevaFila.appendChild(columnaNombre);//a la variable nuevaFila le agragamos los valores
    nuevaFila.appendChild(columnaPrecio);
    nuevaFila.appendChild(columnaStock);
    tbody.appendChild(nuevaFila)//agregamos los valores obtenidos de la nuevaFila al tbody
  }
  tabla.appendChild(tbody);//agregamos todas las filas que tiene tbody a la tabla
  divTabla.appendChild(tabla); //agragamos la tabla al div del html5
}
